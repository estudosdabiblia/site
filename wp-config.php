<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'estudosdabibliawp');

/** MySQL database username */
define('DB_USER', 'estudosdabiblia');

/** MySQL database password */
define('DB_PASSWORD', 'Estudos@123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[+P.^[[GwdeF3p~^/7?X9_@ZJ?N.WeO|p5T3PbDgE<*]3czBbM0y{6{&wmN$9GcB');
define('SECURE_AUTH_KEY',  '.kNvAjx_A;aFC>MpH>+bsB7>{;hH}.Wx&8onPLy[j,^:Xe4+`%q=LzRW#]Xk<Ap[');
define('LOGGED_IN_KEY',    '}l];xe:y^3_K(>M8e=ShK_}+v1r2VF?0PTTl-``64g6=Ub}mq[,^55G]M%YWr[;l');
define('NONCE_KEY',        'iuCK_*Lz{E0{7=L,+@7y*^r]F{cNY[zYo,*)y5`vDL{@Q?0RSenGzo@oWbY?c31G');
define('AUTH_SALT',        'Y6*,9H-2vi&HFHJ#Hu5WZZW/bf&/M>~,!n~^9kj>LG|NHOo%cBPv}cQ*$vdG,K %');
define('SECURE_AUTH_SALT', ':W);y`);%f$JWB3WMV(|g0^!8iF?g9UB-tHcX|;VqQ`s~$120*[@%F6=T%i1FQ05');
define('LOGGED_IN_SALT',   'n/V#GcScEG;at<b(`@&%i>%!U)_H.#0~TNa}?K%`}(SoB@FD7C}@gHN=}-z;!&=$');
define('NONCE_SALT',       '%U;+x-f!&hIUh%eIj?)q1&rw/U&>?bQ;0yG~IjS.Ur7@nb&tq/&UNH*}:<truy`u');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
